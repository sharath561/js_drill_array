//const elements = [1,2,3,4,5]

function map(elements,cb){

    const data = [];

    for (let i=0;i<elements.length;i++){

        data.push(cb(elements[i]))
    }

    return data
}

//let double = map(elements,cb)

//console.log(double)

module.exports = map;
