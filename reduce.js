
//const elements = [1,2,3,4,5];

function reduce(elements,cb,startingValue){

    let acc;
    let curr;

    if (startingValue === undefined){

        acc=elements[0];
        curr = 1;
    }
    else{

        acc = startingValue
        curr = 0
    }

    for (let i = curr ; i<elements.length;i++){

        acc=cb(acc,elements[i])
    }

    return acc
}

//const res = reduce(elements,cb)

//console.log(res)

module.exports = reduce;