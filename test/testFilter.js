
const filter = require('../filter')
const items = [1,2,3,4,5]

function callback(element){
    return element%2 == 0;
}

console.log(filter(items,callback))
