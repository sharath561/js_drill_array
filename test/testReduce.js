const reduce = require('../reduce')
const items = [1,2,3,4,5]

function callback(acc,current){
    return acc+current
}

console.log(reduce(items,callback))